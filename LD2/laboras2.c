#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void timer0_init()
{
    // set up timer with no prescaling
    TCCR0 |= (1 << CS00);
  
    // initialize counter
    TCNT0 = 0;
}
  
int main(void)
{
    // connect led to pin PC0
    //DDRB |= (1 << 0) ;
	DDRC = 0b11111111;
  
    // initialize timer
    timer0_init();
  
    // begalinis ciklas
    while(1)
    {
        // check if the timer count reaches 191
       // if (TCNT0 >= 191)
	   //if (TCNT0 mod 2 ==0 )
       // {
			PORTC = 0b11111111;
            //PORTA ^= (1 << 0);    // toggles the led
            TCNT0 = 0;            // reset counter
      //  }
    }
}
